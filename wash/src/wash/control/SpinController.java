package wash.control;

import actor.ActorThread;
import wash.control.WashingMessage.Order;
import wash.io.WashingIO;
import static wash.control.WashingMessage.Order.*;

public class SpinController extends ActorThread<WashingMessage> {
	/*
	 * • When SpinController receives a SPIN_SLOW message, the barrel should alternate between slow
	 * left rotation and slow right rotation, changing direction every minute.
	 * • WhenSpinControllerreceives a SPIN_FAST message,the barrel should rotate fast(centrifuge).
	 * • WhenSpinControllerreceives a SPIN_OFF message,barrel rotation should stop.
	 * • In all three cases above, an acknowledgment should be sent back (a WashingMessage with com­mand ACKNOWLEDGMENT).
	 */
	private WashingIO io;
	private int mode;
	private long time;
	private Order order;
	private boolean running;

    public SpinController(WashingIO io) {
		this.io = io;
    }

    @Override
    public void run() {
        try {
        	mode = 1;
            time = 0;
            running = false;
            
            while (true) {
                // wait for up to a (simulated) minute for a WashingMessage
                WashingMessage m = receiveWithTimeout(60000 / Settings.SPEEDUP);

                // if m is null, it means a minute passed and no message was received
                if (m != null) {
                    System.out.println("got " + m);
                    order = m.getOrder();

                    if (order == SPIN_SLOW){
                    	running = true;
                    	spin_slow();
                    }
                    if (order == SPIN_FAST){
                    	if (io.getWaterLevel() == 0) { //SF5
                    		running = true;
                        	mode = 4;
                        	io.setSpinMode(mode);
                    	}
                    }
                    if (order == SPIN_OFF){
                    	running = false;
                    	mode = 1;
                    	io.setSpinMode(mode);
                    }
                    
                    WashingMessage ack = new WashingMessage(this, ACKNOWLEDGMENT); 
                    ActorThread<WashingMessage> t = m.getSender();
                    t.send(ack);
                } else {
                	if (order == SPIN_SLOW && running) {
                		spin_slow();
                	} 
                }
                
                
                // ... TODO ...
            }
        } catch (InterruptedException unexpected) {
            // we don't expect this thread to be interrupted,
            // so throw an error if it happens anyway
            throw new Error(unexpected);
        }
    }
    
    private void spin_slow() {
    		int ctime = (int) ((System.currentTimeMillis() - time));

          	if (mode == 1) {
          		mode = 2;
          		io.setSpinMode(mode);
          		time = System.currentTimeMillis();
          		
          	} else if (mode == 2 &&  ctime >= 60000/Settings.SPEEDUP) {
          		mode = 3;
          		io.setSpinMode(mode);
          		time = System.currentTimeMillis();
          		
          	} else if (mode == 3 && ctime >= 60000/Settings.SPEEDUP) {
          		mode = 2;
          		io.setSpinMode(mode);
          		time = System.currentTimeMillis();
          	}
    }
}
