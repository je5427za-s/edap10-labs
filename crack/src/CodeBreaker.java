import java.math.BigInteger;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.Semaphore;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.SwingUtilities;

import client.view.ProgressItem;
import client.view.StatusWindow;
import client.view.WorklistItem;
import network.Sniffer;
import network.SnifferCallback;
import rsa.Factorizer;
import rsa.ProgressTracker;

public class CodeBreaker implements SnifferCallback {

    private final JPanel workList;
    private final JPanel progressList;
        
    private final JProgressBar mainProgressBar;
    
    private ExecutorService pool;

    // -----------------------------------------------------------------------
    
    private CodeBreaker() {
        StatusWindow w  = new StatusWindow();

        workList        = w.getWorkList();
        progressList    = w.getProgressList();
        mainProgressBar = w.getProgressBar();

        pool = Executors.newFixedThreadPool(2); // create new thread pool
        
        w.enableErrorChecks();
    }
    
    // -----------------------------------------------------------------------
    
    public static void main(String[] args) {

        /*
         * Most Swing operations (such as creating view elements) must be performed in
         * the Swing EDT (Event Dispatch Thread).
         * 
         * That's what SwingUtilities.invokeLater is for.
         */

        SwingUtilities.invokeLater(() -> {
            CodeBreaker codeBreaker = new CodeBreaker();
            new Sniffer(codeBreaker).start();
        });     
    }

    // -----------------------------------------------------------------------

    /** Called by a Sniffer thread when an encrypted message is obtained. */
    @Override
    public void onMessageIntercepted(String message, BigInteger n) {
    	SwingUtilities.invokeLater(() -> { // only called once
    	WorklistItem wItem = new WorklistItem(n, message); // create item in list to the left
		workList.add(wItem); 
		
		JButton b = new JButton("Break"); // create new Break-button
    	wItem.add(b); 

    	b.addActionListener(e ->  { // when button is clicked
    		mainProgressBar.setMaximum(mainProgressBar.getMaximum() + 1000000); // increase mainbar's max
    		
    		ProgressItem pItem = new ProgressItem(n, message); // create item in list to the right
    		workList.remove(wItem); // remove from left list
    		progressList.add(pItem); // add to right list
    		
    		Tracker pTrack = new Tracker(pItem, mainProgressBar); // calculates the progress
    		
    		JButton cancel = new JButton("Cancel"); // add stop button
    		pItem.add(cancel);
    		
			JButton delete = new JButton("Delete"); // add delete button once decryption task is completed 

    		Future<?> future = pool.submit(() -> { // sumbit task to pool via lambda 
	    		try {
	    			String encoded = Factorizer.crack(message, n, pTrack); // crack message
	    			SwingUtilities.invokeLater(() -> pItem.getTextArea().setText(encoded)); // set text to decrypted message
	    			
	    			SwingUtilities.invokeLater(() -> pItem.remove(cancel)); // remove cancel button
	    			SwingUtilities.invokeLater(() -> pItem.add(delete)); // add delete button
	    			
	    			//mainProgressBar.setValue(mainProgressBar.getValue()-1000000); // decreasing mainbar's value once task is complete
	    			//mainProgressBar.setMaximum(mainProgressBar.getMaximum()-1000000);// decreasing mainbar's maximum once task is complete
	    			// står i instruktionerna såhär men blir ju fel vid testet
	    			
				} catch (InterruptedException e1) {
					e1.printStackTrace();
				}
			});
    		
    		delete.addActionListener(c ->  { // when delete button is clicked
	    		progressList.remove(pItem); // remove from right list 
	    	});

	    	cancel.addActionListener(c -> { // when cancel button is clicked
	    		future.cancel(true); // cancel task, interrupted thread exception will be thrown?
	    		pTrack.cancel(); // call cancel method in Tracker
	    		pItem.add(delete); // add delete button
	    		pItem.remove(cancel); // remove cancel button
	    		});
	    	 
    	});
    	});
    }
    
    
    /** ProgressTracker: reports how far factorization has progressed */ 
    private class Tracker implements ProgressTracker {
        private int totalProgress = 0;
        private ProgressItem item;
        private JProgressBar mainBar;
        
        public Tracker(ProgressItem item, JProgressBar mainBar) {
        	this.item = item;
        	this.mainBar = mainBar;
        	item.getProgressBar().setMaximum(1000000);
        }
       
        /**
         * Pressed cancel button 
         */
        public void cancel() {
        	item.getTextArea().setText("Cancelled"); // set text to cancelled
        	item.getProgressBar().setMaximum(100); // set max to 100
        	item.getProgressBar().setValue(100); // 100%
            mainBar.setValue(mainBar.getValue() + (1000000 - totalProgress)); // add the remain of progress to mainbar
			//mainProgressBar.setValue(mainProgressBar.getValue()-1000000); // decreasing mainbar's value once task is complete
            //mainProgressBar.setMaximum(mainProgressBar.getMaximum()-1000000);// decreasing mainbar's maximum once task is complete
        }
        
        /**
         * Called by Factorizer to indicate progress. The total sum of
         * ppmDelta from all calls will add upp to 1000000 (one million).
         * 
         * @param  ppmDelta   portion of work done since last call,
         *                    measured in ppm (parts per million)
         */
        @Override
        public void onProgress(int ppmDelta) {
            totalProgress += ppmDelta;
            SwingUtilities.invokeLater(() -> {
            	item.getProgressBar().setValue(totalProgress); // add progress to items progress bar
                mainBar.setValue(mainBar.getValue() + ppmDelta); // add progress to mainbar
                });
            
        }
    }
    
}
