
import clock.AlarmClockEmulator;
import clock.io.ClockInput;
import clock.io.ClockInput.UserInput;
import clock.io.ClockOutput;
import clock.io.SharedMonitor;

public class ClockMain {

    public static void main(String[] args) throws InterruptedException {
        AlarmClockEmulator emulator = new AlarmClockEmulator();
    	
        SharedMonitor mon = new SharedMonitor(0, 0, 0);
    	
        ClockInput  in  = emulator.getInput();
        ClockOutput out = emulator.getOutput();
        
        
        Thread input = new Thread(() -> {
        	while (true) {
            	try {
            		in.getSemaphore().acquire();
					UserInput userInput = in.getUserInput();
		            int choice = userInput.getChoice();
		            
		            if(choice == 1) { // user set new clock time
		            	int h = userInput.getHours();
			            int m = userInput.getMinutes();
			            int s = userInput.getSeconds();
			            
			            mon.setTime(h, m, s, out);
			             
		            } else if(choice == 2) { // user set new alarm time
		            	int h = userInput.getHours();
			            int m = userInput.getMinutes();
			            int s = userInput.getSeconds();
			            
			            mon.setAlarmTime(h, m, s);
			            
		            } else if (choice == 3){ // user pressed both buttons simultaneously	
		            	if (mon.getAlarm()) {
		            		out.setAlarmIndicator(false);
		            		mon.setAlarm(false);
		            		
		            	} else if (mon.checkAlarm()) {
		            		mon.setAlarm(false);
		            		out.setAlarmIndicator(false);
		            		
		            	} else {
		            		out.setAlarmIndicator(true);
		            		mon.setAlarm(true);
		            	}
		            }            		            
            	} catch (InterruptedException e1) {
					e1.printStackTrace();
				}
            }
        });
        
        Thread output = new Thread(() -> {
    		long t0 = System.currentTimeMillis();
        	int i = 0;
			out.displayTime(0, 0, 0);

    		while (true) {
	    		try {
				    long now = System.currentTimeMillis();
					Thread.sleep(1000-(now-t0-i*1000));
					i++;
					mon.increase(out);
					if (mon.checkAlarm()) {
						out.alarm();
						mon.beep();
					}
				} catch (InterruptedException e) {
					e.printStackTrace();
				} 
				  
            }
        });
        
        
        input.start();
        output.start();

    }
    
    
}
