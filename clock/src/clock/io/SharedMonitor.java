package clock.io;

import java.util.concurrent.Semaphore;

public class SharedMonitor {
	private int alarmHours;
	private int alarmMins;
	private int alarmSecs;
	
	private int hours;
	private int mins;
	private int secs;
	
	private boolean alarm;
	private boolean alarmSound;
	private int alarmCounter;
	
	private Semaphore sem; 

	
	public SharedMonitor(int hours, int mins, int secs) {
		this.sem = new Semaphore(1);
		this.hours = hours;
		this.mins = mins;
		this.secs = secs;
		alarm = false;
	}
	
	public void increase(ClockOutput out) throws InterruptedException {
		sem.acquire();
		if(secs < 59) {
			secs++;
		} else if (mins == 59 && secs == 59) {
			if (hours < 23) {
				hours++;
				mins = 0;
				secs = 0;
			} else {
				hours = 0;
				mins = 0;
				secs = 0;
			}
		} else {
			secs = 0;
			mins++;
		}
		out.displayTime(hours, mins, secs);
		sem.release();
	}
	
	public void setTime(int hours, int mins, int secs, ClockOutput out) {
		try {
			sem.acquire();
			this.hours = hours;
			this.mins = mins;
			this.secs = secs;
            out.displayTime(hours, mins, secs);
			sem.release();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public int getHours() {
		return hours;
	}
	
	public int getMins() {
		return mins;
	}
	
	public int getSecs() {
		return secs;
	}
	
	public boolean getAlarm() {
		return alarm;
	}
	
	public void setAlarm(boolean alarm) throws InterruptedException {
		sem.acquire();
		if (!alarm) {
			alarmSound = false;
			alarmCounter = 20;
		}
		this.alarm = alarm;
		sem.release();
	}
	
	public void setAlarmTime(int alarmHours, int alarmMins, int alarmSecs) throws InterruptedException {
		sem.acquire();
		this.alarmHours = alarmHours;
		this.alarmMins = alarmMins;
		this.alarmSecs = alarmSecs;
		sem.release();
	}
	
	/**
	 * 
	 * @return true if time for alarm is in or if alarm is sounding
	 * @throws InterruptedException 
	 */
	public boolean checkAlarm() throws InterruptedException {
		sem.acquire();
		if (hours == alarmHours && mins == alarmMins && secs == alarmSecs && alarm) {
			alarmSound = true;
			alarmCounter = 20;
			sem.release();
			return true;
		} else if(alarmCounter > 0 && alarmSound && alarm) {
			sem.release();
			return true;
		}
		sem.release();
		return false;
		
	}
	
	public void beep() throws InterruptedException {
		sem.acquire();
		alarmCounter --;
		sem.release();
	}

}
