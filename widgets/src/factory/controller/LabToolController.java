package factory.controller;

import factory.model.DigitalSignal;
import factory.model.WidgetKind;
import factory.simulation.Painter;
import factory.simulation.Press;
import factory.swingview.Factory;

/**
 * Implementation of the ToolController interface,
 * to be used for the Widget Factory lab.
 * 
 * @see ToolController
 */
public class LabToolController implements ToolController {
    private final DigitalSignal conveyor, press, paint;
    private final long pressingMillis, paintingMillis;
    private boolean pressOn, paintOn;
   
    
    public LabToolController(DigitalSignal conveyor, DigitalSignal press, DigitalSignal paint, long pressingMillis, long paintingMillis) {
        this.conveyor = conveyor;
        this.press = press;
        this.paint = paint;
        this.pressingMillis = pressingMillis;
        this.paintingMillis = paintingMillis;
        
        
    }
    /**
	 * Starts and stops press when sensor senses green rectangles.
	 * @throws InterruptedException
	 */
    @Override
    public synchronized void onPressSensorHigh(WidgetKind widgetKind) throws InterruptedException {
       
        //
        if (widgetKind == WidgetKind.GREEN_RECTANGULAR_WIDGET) {
        	
        	conveyor.off();
        	startPress();
            press.on(); 
//            Thread.sleep(pressingMillis);
            waitOutside(pressingMillis);
            press.off();
//            Thread.sleep(pressingMillis);
            waitOutside(pressingMillis);
            stopPress();
            startConv();
            
            
            
            
        }
    }
    /**
	 * 
	 * Starts and stops painter when sensor senses orange rounds.
	 * @throws InterruptedException 
	 */
    @Override
    public synchronized void onPaintSensorHigh(WidgetKind widgetKind) throws InterruptedException {
        
        //
        if (widgetKind == WidgetKind.ORANGE_ROUND_WIDGET) {
        	conveyor.off();
        	startPaint();
            paint.on();
//            Thread.sleep(paintingMillis);
            waitOutside(paintingMillis);
            stopPaint();
            paint.off();
            
            startConv();
            
        }
    }
    /**
	 * 
	 * Starts conveyer if neither press or painter is on. 
	 */
    private synchronized void startConv() {
    	if(pressOn == false && paintOn == false) {
    		conveyor.on();
    	}
    }
    
    /**
	 * 
	 * Synchronized methods to starts/stop press/painter and changes respective boolean attriubtes.
	 */
    private synchronized void startPress() {
    	pressOn = true;
    }
    private synchronized void stopPress() {
    	pressOn = false;
    }
    private synchronized void startPaint() {
    	paintOn = true;
    }
    private synchronized void stopPaint() {
    	paintOn = false;
    }
    
    /**
	 * 
	 * Waits for input time (outside of the synchronized methods) 
	 * @throws InterruptedException 
	 */
    private void waitOutside(long millis) throws InterruptedException {
    	long timeToWakeUp = System.currentTimeMillis() + millis;
    	
    	while(timeToWakeUp-System.currentTimeMillis()>0) {
    		long dt = timeToWakeUp-System.currentTimeMillis();
    		wait(dt);
    	}
    }
    
    
    // -----------------------------------------------------------------------
    
    public static void main(String[] args) {
        Factory factory = new Factory();
        ToolController toolController = new LabToolController(factory.getConveyor(),
                                                              factory.getPress(),
                                                              factory.getPaint(),
                                                              Press.PRESSING_MILLIS,
                                                              Painter.PAINTING_MILLIS);
        factory.startSimulation(toolController);
    }
}
