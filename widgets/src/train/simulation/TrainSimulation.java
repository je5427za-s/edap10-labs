package train.simulation;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.LinkedBlockingQueue;

import train.model.Route;
import train.model.Segment;
import train.view.TrainView;
/* We will use trains of length three (figure 2.1). To create such a train, you need write code to
• create a queue,
• load a route, view.loadRoute()
• read three segments from the route, route.next()
• store them in your queue, and
• mark them as busy. segment.enter()

To get the train moving, your code will need to repeatedly
• read the next segment from the route, route.next()
• mark it as busy, head.enter()
• add it to the head of your queue,
• remove the tail Segment from your queue, and 
• mark the removed segment as free. tail.exit() 
2.1.3 Controlling the widget factory
*/

public class TrainSimulation {
	static TrainView view = new TrainView();
    static Monitor mon = new Monitor();

    public static void main(String[] args) {
        newThread();        
        newThread();
        newThread();
        newThread();
        newThread();
        newThread();
        newThread();
        newThread();
        newThread();
        newThread();
        newThread();
        newThread();


        
    }
    
    private static void newThread() {
        Thread t = new Thread(() -> {
        	ConcurrentLinkedQueue<Segment> train = new ConcurrentLinkedQueue<Segment>();
            
            Route route = view.loadRoute();
            
            Segment s1 = route.next();
            Segment s2 = route.next();
            Segment s3 = route.next();

            train.add(s1);
            train.add(s2);
            train.add(s3);

            try {
            	mon.enter(s1);
            	mon.enter(s2);
				mon.enter(s3);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
            
            while(true) {
            	Segment sNew = route.next();
            	try {
					mon.enter(sNew);
	            	train.add(sNew);
	            	Segment sTail = (Segment) train.poll();
	    			sTail.exit();
	            	mon.exit(sTail);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

            }
 	
        });
        t.start();

    }

}
