package train.simulation;

import java.util.HashSet;
import java.util.Set;

import train.model.Segment;

/* 	• noting a segment as busy,
	• noting a segment as free, and
	• blocking until a given segment s is free.
*/
public class Monitor {
	private Set<Segment> segments;
	
	public Monitor() {
		segments = new HashSet<Segment>();
	}
		
	public synchronized void enter(Segment s) throws InterruptedException {
		while (segments.contains(s)) {
			wait();
		} 
		segments.add(s);
		s.enter();
	}
	
	public synchronized void exit(Segment s) throws InterruptedException {
		segments.remove(s);
		notifyAll();
	}
	
}
